package Client;

import java.io.Serializable;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */
public class MessageType implements Serializable{
    private final String MESSAGE = "MESSAGE";
    private final String NEW_USER = "NEW_USER";
    private final String ONLINE_USERS_COUNT = "ONLINE_USERS_COUNT";
    private final String DELETE_USER = "DELETE_USER";
    private String type;

    public String getMESSAGE() {
        return MESSAGE;
    }

    public String getNEW_USER() {
        return NEW_USER;
    }

    public String getONLINE_USERS_COUNT() {
        return ONLINE_USERS_COUNT;
    }

    public String getType() {
        return type;
    }

    public void setMessageType(String s){
        switch (s){
            case "MESSAGE":
                type = MESSAGE;
                break;
            case "NEW_USER":
                type = NEW_USER;
                break;
            case "ONLINE_USERS_COUNT":
                type = ONLINE_USERS_COUNT;
                break;
            case "DELETE_USER":
                type = DELETE_USER;
                break;
        }
    }
}