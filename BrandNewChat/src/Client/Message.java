package Client;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */
public class Message implements Serializable{
    private String message;
    private User user;
    private int usersCount;
    private ArrayList<User> users;
    private MessageType messageType = new MessageType();

    public void setMessageType(String type) {
        messageType.setMessageType(type);
    }
    public String getMessageType() {
        return messageType.getType();
    }

    public ArrayList<User> getUsers() {
        return users;
    }
    public void setUsers(ArrayList<User> users) {
        this.users = users;
    }

    public int getUsersCount() {
        return usersCount;
    }
    public void setUsersCount(int usersCount) {
        this.usersCount = usersCount;
    }

    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }

    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }
}
