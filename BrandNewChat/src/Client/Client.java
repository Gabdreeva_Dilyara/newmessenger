package Client;

import Client.Chat.ChatController;
import Client.Login.LoginController;

import java.io.*;
import java.net.Socket;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *
 */
public class Client implements Runnable  {
    private String userName;
    private Socket socket;
    private ObjectInputStream reader;
    private static ObjectOutputStream writer;
    private ChatController chatController;

    private final String MESSAGE = "MESSAGE";
    private final String NEW_USER = "NEW_USER";
    private final String ONLINE_USERS_COUNT = "ONLINE_USERS_COUNT";
    private final String DELETE_USER = "DELETE_USER";

    public Client(ChatController chatController, String userName){
            this.chatController = chatController;
            this.userName = userName;
    }
   /* private void sendFile() {
        File file = chatController.getFile();
        try {
            writer.writeObject(file);
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    } */
    private void sendNewUser() { //при подключении нового пользователя, отсылает серверу сообщение о новом пользователе и и этого пользователя
        User user = new User();
        user.setName(userName);
        chatController.setUser(user);

        Message userMessage = new Message();
        userMessage.setUser(user);
        userMessage.setMessageType(NEW_USER);
        try {
            writer.writeObject(userMessage);
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void setOnlineUsers(Message message){ //меняем счет нового пользователя
        chatController.setOnlineLabel(message);
        chatController.addNewUserToList(message.getUsers());
    }
    @Override
    public void run() {
        boolean running =true;
            try {
                socket = new Socket("127.0.0.1", 5000);
                LoginController.getInstance().showScene();
                InputStream is = socket.getInputStream();
                reader = new ObjectInputStream(is);
                OutputStream os = socket.getOutputStream();
                writer = new ObjectOutputStream(os);

                sendNewUser();

                while (socket.isConnected() && running) {
                    Message message = (Message) reader.readObject();
                    if (message != null) {
                        switch (message.getMessageType()) {
                            case MESSAGE:
                                chatController.addToChat(message);
                                break;
                            case ONLINE_USERS_COUNT:
                                setOnlineUsers(message);
                                break;
                            /*case DELETE_USER:
                                if (userName.equals(message.getUser().getName())) {
                                    running = false;
                                }
                                else {
                                    chatController.deleteUserFromList(message.getUser());
                                }*/
                        }

                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    public static void send(Message msg){
        System.out.println("NEW MESSAGE >>   " + msg.getMessage());
        try {
            writer.writeObject(msg);
            writer.reset();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
