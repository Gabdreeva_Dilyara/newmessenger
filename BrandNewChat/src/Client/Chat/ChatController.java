package Client.Chat;

import Client.Client;
import Client.Message;
import Client.User;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */

public class ChatController implements Initializable{
    private User user;
    @FXML private TextArea messageBox;
    @FXML private Label usernameLabel;
    @FXML private Label onlineCountLabel;
    @FXML private ListView chatPane;
    @FXML private ListView userList;
    @FXML private BorderPane borderPane;

    private double xOffset;
    private double yOffset;


    public void sendButtonAction() throws IOException {
        String msg = messageBox.getText();
        Message message = new Message();
        message.setMessage(msg);
        message.setUser(user);
        message.setUsersCount(user.getOnlineUsers());
        message.setMessageType("MESSAGE");
        if (!messageBox.getText().isEmpty()) {
            messageBox.clear();
            Client.send(message);
        }
    }
    public void setOnlineLabel(Message message) {
        String userCount = String.valueOf(message.getUsersCount());
        Platform.runLater(
                () -> onlineCountLabel.setText(userCount)
        );
    }
    public synchronized void addToChat(Message message) {
        Task<HBox> myMessage = new Task<HBox>() {
            @Override
            public HBox call() throws Exception {
                Label label = new Label(message.getMessage());
                HBox hBox = new HBox();
                hBox.getChildren().addAll(label);
                hBox.setAlignment(Pos.BASELINE_LEFT);
                return hBox;
            }
        };
        myMessage.setOnSucceeded(event -> {
            chatPane.getItems().add(myMessage.getValue());
        });

        Task<HBox> otherMessage = new Task<HBox>() {
            @Override
            public HBox call() throws Exception {
                Label label = new Label(message.getUser().getName() + ": " + message.getMessage());
                HBox hBox = new HBox();
                label.setStyle("-fx-background-color:#acc6ff;");
                hBox.getChildren().addAll(label);
                hBox.setAlignment(Pos.BASELINE_RIGHT);
                return hBox;
            }
        };
        myMessage.setOnSucceeded(event -> {
            chatPane.getItems().add(myMessage.getValue());
        });
        otherMessage.setOnSucceeded(event -> {
            chatPane.getItems().add(otherMessage.getValue());
        });
        if (message.getUser().getName().equals(usernameLabel.getText())) {
            Thread thread = new Thread(myMessage);
            thread.setDaemon(true);
            thread.start();
        }
        else {
            Thread thread = new Thread(otherMessage);
            thread.setDaemon(true);
            thread.start();
        }
    }
    public void setUsernameLabel(String username) {
        usernameLabel.setText(username);
    }
    public void sendMethod(KeyEvent event) throws IOException {
        if (event.getCode() == KeyCode.ENTER) {
            sendButtonAction();
        }
    }
    public void initialize(URL location, ResourceBundle resources) {
        messageBox.addEventFilter(KeyEvent.KEY_PRESSED, ke -> {
            if (ke.getCode().equals(KeyCode.ENTER)) {
                try {
                    sendButtonAction();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                ke.consume();
            }
        });
    }
    public void setUser(User user) {
        this.user = user;
    }
    public void addNewUserToList(ArrayList<User> users) {
        users.forEach(user1 -> System.out.println(user1.getName()));
        ArrayList<String> arrayList = new ArrayList<>();
        users.forEach(user1 -> arrayList.add(user1.getName()));
        ObservableList<String> list = FXCollections.observableArrayList();
        for (String name : arrayList) {
            list.add(name);
        }
        Platform.runLater(
                () -> userList.setItems(list)
        );
    }

}
