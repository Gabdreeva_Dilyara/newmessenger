package Client.Login;

import Client.Chat.ChatController;
import Client.Client;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class LoginController implements Initializable{
    @FXML private TextField usernameTextfield;
    public static ChatController chatController;
    @FXML private BorderPane borderPane;
    private double xOffset;
    private double yOffset;
    private Scene scene;

    private static LoginController instance;

    public LoginController() {
        instance = this;
    }

    public static LoginController getInstance() {
        return instance;
    }

    public void loginButtonAction() throws IOException {
        String username = usernameTextfield.getText();
        FXMLLoader fmxlLoader = new FXMLLoader(getClass().getResource("ChatView.fxml"));
        Parent window = (Pane) fmxlLoader.load();
        chatController = fmxlLoader.getController();
        Client listener = new Client(chatController, username);
        Thread thread = new Thread(listener);
        thread.start();
        scene = new Scene(window);
    }
    public void sendMethod(KeyEvent event) throws IOException {
        if (event.getCode() == KeyCode.ENTER) {
            loginButtonAction();
        }
    }
    public void showScene() throws IOException {
        Platform.runLater(() -> {
            Stage stage = (Stage) usernameTextfield.getScene().getWindow();
            stage.setResizable(true);
            stage.setWidth(1105);
            stage.setHeight(665);

            stage.setScene(this.scene);
            stage.centerOnScreen();
            chatController.setUsernameLabel(usernameTextfield.getText());
            stage.setOnCloseRequest(e ->  {
                Platform.exit();
                //chatController.removeUser();
            });
        });
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        borderPane.setOnMousePressed(event -> {
            xOffset = MainLauncher.getPrimaryStage().getX() - event.getScreenX();
            yOffset = MainLauncher.getPrimaryStage().getY() - event.getScreenY();
            borderPane.setCursor(Cursor.CLOSED_HAND);
        });

        borderPane.setOnMouseDragged(event -> {
            MainLauncher.getPrimaryStage().setX(event.getScreenX() + xOffset);
            MainLauncher.getPrimaryStage().setY(event.getScreenY() + yOffset);

        });

        borderPane.setOnMouseReleased(event -> {
            borderPane.setCursor(Cursor.DEFAULT);
        });

    }
}
