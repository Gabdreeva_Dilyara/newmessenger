import Client.Message;
import Client.User;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */
public class Server {
    public static void main(String[] args) {
        new Server().go();
    }

    private static ArrayList<ObjectOutputStream> writers = new ArrayList<>();
    private final int PORT = 5000;
    private static ArrayList<User> users = new ArrayList<>();
    private static int onlineUsers;
    private ObjectOutputStream writer;

    private final String MESSAGE = "MESSAGE";
    private final String NEW_USER = "NEW_USER";
    private final String ONLINE_USERS_COUNT = "ONLINE_USERS_COUNT";

    public void go() {
        try {
            ServerSocket serverSocket = new ServerSocket(PORT);
            while (true) {
                Socket socket = serverSocket.accept();
                System.out.println("Got new User");
                OutputStream os = socket.getOutputStream();
                writer = new ObjectOutputStream(os);
                writers.add(writer);
                Thread listener = new Thread(new Listener(socket));
                listener.start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private static void tellEveryone(Message message) {
        for (ObjectOutputStream writer: writers){
            try {
                writer.writeObject(message);
                writer.reset();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    private void setNewUser(Message message) {
        User user = message.getUser();
        users.add(user);
        onlineUsers = users.size();
        for (User connectedUser : users) {
            connectedUser.setOnlineUsers(onlineUsers);
        }
    }
    private void setOnlineUsersCount() {
        Message usersOnlineCount = new Message();
        usersOnlineCount.setUsersCount(onlineUsers);
        usersOnlineCount.setMessageType(ONLINE_USERS_COUNT);
        usersOnlineCount.setUsers(users);
        tellEveryone(usersOnlineCount);
    }
    private class Listener implements Runnable{
        Socket socket;
        private ObjectInputStream reader;
        Listener(Socket socket) {
            this.socket = socket;
        }
        @Override
        public void run() {
            try {
                InputStream is = socket.getInputStream();
                reader = new ObjectInputStream(is);

                while (socket.isConnected()) {
                    Message message = (Message) reader.readObject();
                    if (message != null) {
                        switch (message.getMessageType()) {
                            case NEW_USER:
                                setNewUser(message);
                                setOnlineUsersCount();
                                break;
                            case MESSAGE:
                                tellEveryone(message);
                                break;
                        }
                    }
                }
                } catch(ClassNotFoundException e){
                    e.printStackTrace();
                } catch(IOException e){
                    e.printStackTrace();
                }
            }
        }
}
